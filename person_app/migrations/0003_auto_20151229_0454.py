# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person_app', '0002_auto_20151229_0453'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='zip_code',
            field=models.CharField(help_text='What is your zip_code?', max_length=10),
        ),
    ]
