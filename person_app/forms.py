from django import forms
from .models import Person


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = ['first_name', 'last_name', 'date_of_birth', 'zip_code']
        exclude = []
        widgets = None
        localized_fields = None
        labels = {}
        help_texts = {}
        error_messages = {}

    def __init__(self, *args, **kwargs):
        return super(PersonForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        return super(PersonForm, self).is_valid()

    def full_clean(self):
        return super(PersonForm, self).full_clean()

    def clean_first_name(self):
        first_name = self.cleaned_data.get("first_name", None)
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get("last_name", None)
        return last_name

    def clean_date_of_birth(self):
        date_of_birth = self.cleaned_data.get("date_of_birth", None)
        return date_of_birth

    def clean_zip_code(self):
        zip_code = self.cleaned_data.get("zip_code", None)
        return zip_code

    def clean(self):
        return super(PersonForm, self).clean()

    def validate_unique(self):
        return super(PersonForm, self).validate_unique()

    def save(self, commit=True):
        return super(PersonForm, self).save(commit)
