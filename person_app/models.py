# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Person(models.Model):
	"""A model to store a list of persons. """
	first_name = models.CharField(max_length=255, help_text='What is your first name?', verbose_name='First Name')
	last_name = models.CharField(max_length=255, help_text='What is your last name?', verbose_name='Last Name')
	date_of_birth = models.DateField(auto_now=False, auto_now_add=False, help_text='When were you born? Format: mm/dd/yyyy', verbose_name='Date of Birth')
	zip_code = models.CharField(max_length=10, help_text='What is your zip code?', verbose_name='Zip Code')