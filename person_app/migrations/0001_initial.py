# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(help_text='What is you first name?', max_length=255, verbose_name='First Name')),
                ('last_name', models.CharField(help_text='What is you last name?', max_length=255, verbose_name='Last Name')),
                ('date_of_birth', models.DateField()),
                ('zip_code', models.IntegerField()),
            ],
        ),
    ]
