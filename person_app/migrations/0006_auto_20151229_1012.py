# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person_app', '0005_auto_20151229_0730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='date_of_birth',
            field=models.DateField(help_text='When were you born? Format: mm/dd/yyyy', verbose_name='Date of Birth'),
        ),
    ]
