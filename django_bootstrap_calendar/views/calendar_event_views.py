from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from ..models import CalendarEvent
from ..forms import CalendarEventForm
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.http import Http404



# -*- coding: utf-8 -*-
__author__ = 'sandlbn and w3lly'

from django.views.generic import TemplateView
#from models import CalendarEvent
from ..serializers import event_serializer
from ..utils import timestamp_to_datetime

import datetime


class CalendarJsonListView(ListView):

    template_name = 'django_bootstrap_calendar/calendar_events.html'

    def get_queryset(self):
        queryset = CalendarEvent.objects.filter()
        from_date = self.request.GET.get('from', False)
        to_date = self.request.GET.get('to', False)

        if from_date and to_date:
            queryset = queryset.filter(
                start__range=(
                    timestamp_to_datetime(from_date) + datetime.timedelta(-30),
                    timestamp_to_datetime(to_date)
                    )
            )
        elif from_date:
            queryset = queryset.filter(
                start__gte=timestamp_to_datetime(from_date)
            )
        elif to_date:
            queryset = queryset.filter(
                end__lte=timestamp_to_datetime(to_date)
            )

        return event_serializer(queryset)


class CalendarView(TemplateView):

    template_name = 'django_bootstrap_calendar/calendar.html'




class CalendarEventListView(ListView):
    model = CalendarEvent
    template_name = "django_bootstrap_calendar/calendar_event_list.html"
    paginate_by = 20
    context_object_name = "calendar_event_list"
    allow_empty = True
    page_kwarg = 'page'
    paginate_orphans = 0

    def __init__(self, **kwargs):
        return super(CalendarEventListView, self).__init__(**kwargs)

    def dispatch(self, *args, **kwargs):
        return super(CalendarEventListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(CalendarEventListView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        return super(CalendarEventListView, self).get_queryset()

    def get_allow_empty(self):
        return super(CalendarEventListView, self).get_allow_empty()

    def get_context_data(self, *args, **kwargs):
        ret = super(CalendarEventListView, self).get_context_data(*args, **kwargs)
        return ret

    def get_paginate_by(self, queryset):
        return super(CalendarEventListView, self).get_paginate_by(queryset)

    def get_context_object_name(self, object_list):
        return super(CalendarEventListView, self).get_context_object_name(object_list)

    def paginate_queryset(self, queryset, page_size):
        return super(CalendarEventListView, self).paginate_queryset(queryset, page_size)

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return super(CalendarEventListView, self).get_paginator(queryset, per_page, orphans=0, allow_empty_first_page=True)

    def render_to_response(self, context, **response_kwargs):
        return super(CalendarEventListView, self).render_to_response(context, **response_kwargs)

    def get_template_names(self):
        return super(CalendarEventListView, self).get_template_names()


class CalendarEventDetailView(DetailView):
    model = CalendarEvent
    template_name = "django_bootstrap_calendar/calendar_event_detail.html"
    context_object_name = "calendar_event"
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    pk_url_kwarg = 'pk'

    def __init__(self, **kwargs):
        return super(CalendarEventDetailView, self).__init__(**kwargs)

    def dispatch(self, *args, **kwargs):
        return super(CalendarEventDetailView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(CalendarEventDetailView, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return super(CalendarEventDetailView, self).get_object(queryset)

    def get_queryset(self):
        return super(CalendarEventDetailView, self).get_queryset()

    def get_slug_field(self):
        return super(CalendarEventDetailView, self).get_slug_field()

    def get_context_data(self, **kwargs):
        ret = super(CalendarEventDetailView, self).get_context_data(**kwargs)
        return ret

    def get_context_object_name(self, obj):
        return super(CalendarEventDetailView, self).get_context_object_name(obj)

    def render_to_response(self, context, **response_kwargs):
        return super(CalendarEventDetailView, self).render_to_response(context, **response_kwargs)

    def get_template_names(self):
        return super(CalendarEventDetailView, self).get_template_names()


class CalendarEventCreateView(CreateView):
    model = CalendarEvent
    form_class = CalendarEventForm
    fields = ['title', 'url', 'css_class', 'start', 'end']
    template_name = "django_bootstrap_calendar/calendar_event_create.html"
    success_url = reverse_lazy("calendar_event_list")

    def __init__(self, **kwargs):
        return super(CalendarEventCreateView, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        return super(CalendarEventCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(CalendarEventCreateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(CalendarEventCreateView, self).post(request, *args, **kwargs)

    def get_form_class(self):
        return super(CalendarEventCreateView, self).get_form_class()

    def get_form(self, form_class):
        return super(CalendarEventCreateView, self).get_form(form_class)

    def get_form_kwargs(self, **kwargs):
        return super(CalendarEventCreateView, self).get_form_kwargs(**kwargs)

    def get_initial(self):
        return super(CalendarEventCreateView, self).get_initial()

    def form_invalid(self, form):
        return super(CalendarEventCreateView, self).form_invalid(form)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        return super(CalendarEventCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        ret = super(CalendarEventCreateView, self).get_context_data(**kwargs)
        return ret

    def render_to_response(self, context, **response_kwargs):
        return super(CalendarEventCreateView, self).render_to_response(context, **response_kwargs)

    def get_template_names(self):
        return super(CalendarEventCreateView, self).get_template_names()

    def get_success_url(self):
        return reverse("calendar_event_detail", args=(self.object.pk,))


class CalendarEventUpdateView(UpdateView):
    model = CalendarEvent
    form_class = CalendarEventForm
    # fields = ['title', 'url', 'css_class', 'start', 'end']
    template_name = "django_bootstrap_calendar/calendar_event_update.html"
    initial = {}
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    pk_url_kwarg = 'pk'
    context_object_name = "calendar_event"

    def __init__(self, **kwargs):
        return super(CalendarEventUpdateView, self).__init__(**kwargs)

    def dispatch(self, *args, **kwargs):
        return super(CalendarEventUpdateView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(CalendarEventUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(CalendarEventUpdateView, self).post(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return super(CalendarEventUpdateView, self).get_object(queryset)

    def get_queryset(self):
        return super(CalendarEventUpdateView, self).get_queryset()

    def get_slug_field(self):
        return super(CalendarEventUpdateView, self).get_slug_field()

    def get_form_class(self):
        return super(CalendarEventUpdateView, self).get_form_class()

    def get_form(self, form_class):
        return super(CalendarEventUpdateView, self).get_form(form_class)

    def get_form_kwargs(self, **kwargs):
        return super(CalendarEventUpdateView, self).get_form_kwargs(**kwargs)

    def get_initial(self):
        return super(CalendarEventUpdateView, self).get_initial()

    def form_invalid(self, form):
        return super(CalendarEventUpdateView, self).form_invalid(form)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        return super(CalendarEventUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        ret = super(CalendarEventUpdateView, self).get_context_data(**kwargs)
        return ret

    def get_context_object_name(self, obj):
        return super(CalendarEventUpdateView, self).get_context_object_name(obj)

    def render_to_response(self, context, **response_kwargs):
        return super(CalendarEventUpdateView, self).render_to_response(context, **response_kwargs)

    def get_template_names(self):
        return super(CalendarEventUpdateView, self).get_template_names()

    def get_success_url(self):
        return reverse("calendar_event_detail", args=(self.object.pk,))


class CalendarEventDeleteView(DeleteView):
    model = CalendarEvent
    template_name = "django_bootstrap_calendar/calendar_event_delete.html"
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    pk_url_kwarg = 'pk'
    context_object_name = "calendar_event"

    def __init__(self, **kwargs):
        return super(CalendarEventDeleteView, self).__init__(**kwargs)

    def dispatch(self, *args, **kwargs):
        return super(CalendarEventDeleteView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        raise Http404

    def post(self, request, *args, **kwargs):
        return super(CalendarEventDeleteView, self).post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return super(CalendarEventDeleteView, self).delete(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return super(CalendarEventDeleteView, self).get_object(queryset)

    def get_queryset(self):
        return super(CalendarEventDeleteView, self).get_queryset()

    def get_slug_field(self):
        return super(CalendarEventDeleteView, self).get_slug_field()

    def get_context_data(self, **kwargs):
        ret = super(CalendarEventDeleteView, self).get_context_data(**kwargs)
        return ret

    def get_context_object_name(self, obj):
        return super(CalendarEventDeleteView, self).get_context_object_name(obj)

    def render_to_response(self, context, **response_kwargs):
        return super(CalendarEventDeleteView, self).render_to_response(context, **response_kwargs)

    def get_template_names(self):
        return super(CalendarEventDeleteView, self).get_template_names()

    def get_success_url(self):
        return reverse("calendar_event_list")
