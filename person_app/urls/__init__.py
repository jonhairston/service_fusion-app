from django.conf.urls import patterns, include

urlpatterns = patterns('',

    (r'^', include('person_app.urls.person_urls')),  # NOQA
)
