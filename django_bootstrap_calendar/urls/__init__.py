from django.conf.urls import patterns, include

urlpatterns = patterns('',

    (r'^', include('django_bootstrap_calendar.urls.calendar_event_urls')),  # NOQA
)
