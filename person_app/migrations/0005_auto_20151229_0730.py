# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person_app', '0004_auto_20151229_0651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='first_name',
            field=models.CharField(help_text='What is your first name?', max_length=255, verbose_name='First Name'),
        ),
        migrations.AlterField(
            model_name='person',
            name='last_name',
            field=models.CharField(help_text='What is your last name?', max_length=255, verbose_name='Last Name'),
        ),
    ]
