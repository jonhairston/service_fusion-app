from django import forms
from .models import CalendarEvent


class CalendarEventForm(forms.ModelForm):

    class Meta:
        model = CalendarEvent
        fields = ['title', 'url', 'css_class', 'start', 'end']
        exclude = []
        widgets = None
        localized_fields = None
        labels = {}
        help_texts = {}
        error_messages = {}

    def __init__(self, *args, **kwargs):
        return super(CalendarEventForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        return super(CalendarEventForm, self).is_valid()

    def full_clean(self):
        return super(CalendarEventForm, self).full_clean()

    def clean_title(self):
        title = self.cleaned_data.get("title", None)
        return title

    def clean_url(self):
        url = self.cleaned_data.get("url", None)
        return url

    def clean_css_class(self):
        css_class = self.cleaned_data.get("css_class", None)
        return css_class

    def clean_start(self):
        start = self.cleaned_data.get("start", None)
        return start

    def clean_end(self):
        end = self.cleaned_data.get("end", None)
        return end

    def clean(self):
        return super(CalendarEventForm, self).clean()

    def validate_unique(self):
        return super(CalendarEventForm, self).validate_unique()

    def save(self, commit=True):
        return super(CalendarEventForm, self).save(commit)
