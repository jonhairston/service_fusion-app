from django.conf.urls import patterns, url
from ..views import (CalendarEventListView, CalendarEventCreateView, CalendarEventDetailView,
                     CalendarEventUpdateView, CalendarEventDeleteView, CalendarJsonListView, CalendarView)

from django.contrib.auth.decorators import login_required


urlpatterns = patterns('',
    url(r'^json/$', CalendarJsonListView.as_view(), name='calendar_json'),
    url( r'^$',CalendarView.as_view(), name='calendar'),

    url(r'^create/$',  # NOQA
        login_required(CalendarEventCreateView.as_view()),
        name="calendar_event_create"),

    url(r'^(?P<pk>\d+)/update/$',
        login_required(CalendarEventUpdateView.as_view()),
        name="calendar_event_update"),

    url(r'^(?P<pk>\d+)/delete/$',
        login_required(CalendarEventDeleteView.as_view()),
        name="calendar_event_delete"),

    url(r'^(?P<pk>\d+)/$',
        CalendarEventDetailView.as_view(),
        name="calendar_event_detail"),

    url(r'^list/$',
        CalendarEventListView.as_view(),
        name="calendar_event_list"),
)
